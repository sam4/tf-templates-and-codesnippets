#Resource group
resource "azurerm_resource_group" "rg" {
    name     = "${var.stage}-we-${var.appname}-01-rg"
    tags     = {
      "Department" = var.department
    }
    location = var.azure_location_westeurope
}

# !!! Do NOT MAKE ANY CHANGES after this line !!!
#---------------------------------------------------------------------------------------------------------

#Role Assignment
#IDs of AAD groups are taken from environment variables exported from repository variables
resource "azurerm_role_assignment" "reader" {
  scope                = azurerm_resource_group.rg.id
  role_definition_name = "Reader"
  principal_id         = var.grp_sec_app_readonly_id #GRP-SEC-RG-TestRepo01-ReadOnly
}

resource "azurerm_role_assignment" "contributor" {
  count = var.stage == "d" ? 1 : 0 #Only assign role to group in DEV
  scope                = azurerm_resource_group.rg.id
  role_definition_name = "Contributor"
  principal_id         = var.grp_sec_app_contribute_id #GRP-SEC-RG-TestRepo01-Contribute
}

resource "azurerm_role_assignment" "owner" {
  count = var.stage == "d" ? 1 : 0 #Only assign role to group in DEV
  scope                = azurerm_resource_group.rg.id
  role_definition_name = "Owner"
  principal_id         = var.grp_sec_app_admin_id #GRP-SEC-RG-TestRepo01-Admin
}

resource "azurerm_role_assignment" "breakglass" {
  count = var.stage == "d" ? 0 : 1 #Only assign role to group in UAT and PROD
  scope                = azurerm_resource_group.rg.id
  role_definition_name = "Owner"
  principal_id         = var.grp_sec_app_breakglass_id #GRP-SEC-RG-TestRepo01-BreakGlass
}