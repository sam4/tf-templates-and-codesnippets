#******************************************************************
# Start here with editing: Project specific variables             *
#******************************************************************
# Azure SQL Server
variable "azure_sql_admin_pw" {
  type = string
}

# Azure SQL AAD Admin
variable "azure_sql_aad_admin_email" {
  type = string
}
variable "azure_sql_aad_object_id" {
  type = string
}

# Azure SQL Database
variable "max_db_size" {
  type = string
}

#******************************************************************
# End here with edititng: Project specific variables              *
#******************************************************************
