# README #

## What is this repository for? ##

* This repository provides templates and code snippets for a fast and easy Terraform (IaC) development of cloud resources and services

## Structure ##
Since this repository is more of a knowledge base/template collection than a classical code repository, it is structured as such. The goal of the structure is to gain a very quick overview and an easy understanding of the usage of the provided artifacts.

* 00_MainTemplate
    * /config
        * d.tfvars
        * q.tfvars
        * p.tfvars
    * /modules/main_application
        * input.tf
        * main.tf
    * bitbucket-pipelines.yml
    * terraform.tfvars
    * input.tf
    * main.tf
* Topic1Folder
    * Topic1Name-ReadMe.md
    * Topic1Name-bitbucket-pipelines.yml
    * Topic1Name.tf
    * Topic1Name-input.tf
    * Topic1Name-main.tf
* Topic2Folder
    * Topic2Name-ReadMe.md
    * Topic2Name-bitbucket-pipelines.yml
    * Topic2Name.tf
    * Topic2Name-input.tf
    * Topic2Name-main.tf

### Example for Azure SQL ###
* AzureSql
    * AzureSql-ReadMe.md
    * AzureSql-bitbucket-pipelines.yml
    * AzureSql.tf
    * AzureSql-input.tf
    * AzureSql-main.tf

## How to set it up? ##

* This repository is not meant to be built or compiled in any way! Its only purpose is to provide help in form of templates and code snippets to be copied and pasted into your own Terraform project.

## How to use this repository
### Starting a Terraform project from scratch
1. Consult the page [Engineering, development and deployment of a new cloud service](https://amconfluence.slcloud.ch/display/AXS/Engineering%2C+development+and+deployment+of+a+new+cloud+service "Engineering, development and deployment of a new cloud service") and follow the steps there
    1. A TF repository using the skeleton structure from the folder "00_MainTemplate" will be created for you
    1. All relevant [repository/workspace variables](https://amconfluence.slcloud.ch/display/AXS/Terraform+Repositories+at+SwissLife) will be set up for you
### Working on an existing project
1. After recieving the link to the TF repository you can start using this repository
    1. Setup/Adjust project specific settings using [project specific repository variables](https://amconfluence.slcloud.ch/display/AXS/Terraform+Deployment+Variables#TerraformDeploymentVariables-Recommendedprojectspecificvariables)
    1. Use Topic folders to copy the needed source code into your project
        1. Topic-ReadMe.md: Always consult this file first to get important information
        1. Topic.tf: This is the Terraform source code that can be copied over to your project
        1. Topic-bitbucket-pipelines.yml: Shows all topic related changes and inputs to the pipeline
        1. Topic-input.tf: Shows all topic related variable inputs (can be used for root and modules directory)
        1. Topic-main.tf: Shows all topic related variable forwarding to the modules

## Contribution guidelines ##
* Terraform repositories use the [GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html "GitLab Flow documentation") by default! Please always consider this fact while working on a tf-repo, since the branche names are different than the ones in Git flow!
    * The master branch is used as the main and development branch at the same time
    * Well tested and working code can be committed into a feature branch (There is no dev branch by design)
    * Then make a pull request to get the code being included into master

## Who do I talk to? ##

* Repo owner for general questions
* Contact contributors directly if you have a secific question