#DB server
resource "azurerm_sql_server" "azuresql-dbserver-01" {
  name = "${var.stage}-we-${var.appname}-01-rg-dbserver-01"
  location = var.azure_location_westeurope
  resource_group_name = azurerm_resource_group.rg.name #Name of the main resource group
  version = "12.0"
  administrator_login = "${var.appname}DbAdmin"
  administrator_login_password = var.azure_sql_admin_pw #Stage-dependent repository variable must be defined in Bitbucket
  tags = { 
    "Department" = "${var.department}"
  }
}

#Set AAD admin
resource "azurerm_sql_active_directory_administrator" "ad_admin" {
  server_name         = azurerm_sql_server.azuresql-dbserver-01.name #Name of the SQL server defined in related section
  resource_group_name = azurerm_resource_group.rg.name #Name of the main resource group
  login               = var.azure_sql_aad_admin_email #A valid e-mail address needs to be provided
  tenant_id           = var.azure_tenant_id
  object_id           = "object_id" #Request the object id for the provided login from the AXS team
}

resource "azurerm_sql_database" "azuresql-db-01" {
  name = "${var.stage}-we-${var.appname}-01-rg-db-01"
  location = var.azure_location_westeurope
  resource_group_name = azurerm_resource_group.rg.name #Name of the main resource group
  server_name = azurerm_sql_server.azuresql-dbserver-01.name #Name of the SQL server defined in related section
  #Optional parameter, uncomment to use it
  #max_size_bytes = var.max_db_size
  collation= "SQL_Latin1_General_CP1_CI_AS"
  edition= "GeneralPurpose"
  requested_service_objective_name= "GP_Gen5_2"
  create_mode= "Default"
  tags = {
    "Department" = "${var.department}"
  }
}