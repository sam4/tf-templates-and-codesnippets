# Azure SQL #

## Supported resources
All the listed resources are tested with the provided parameters in the template code only! If you need additional parameters, please consult the Terraform documentation for [Azure](https://www.terraform.io/docs/providers/azurerm/index.html).

### Azure SQL Server ([Terraform documentation](https://www.terraform.io/docs/providers/azurerm/d/sql_server.html))
This resource needs to have user specific repository variables being defined and [passed over as an environment variable](https://amconfluence.slcloud.ch/display/AXS/Terraform+Repositories+at+SwissLife).
* DB Admin password
    * TF attribute: administrator_login_password
    * EnvVar in pipeline YAML: TF_VAR_azure_sql_admin_pw
    * Bitbucket repository variables: D/Q/P_AZURE_SQL_ADMIN_PW

### Azure SQL AAD Admin ([Terraform documentation](https://www.terraform.io/docs/providers/azurerm/r/sql_active_directory_administrator.html))
This resource needs to have user specific repository variables being defined and [passed over as an environment variable](https://amconfluence.slcloud.ch/display/AXS/Terraform+Repositories+at+SwissLife).
* E-Mail address of the AAD Admin
    * TF attribute: login
    * EnvVar in pipeline YAML: TF_VAR_azure_sql_aad_admin_email
    * Bitbucket repository variables: D/Q/P_AZURE_SQL_AAD_ADMIN_EMAIL
* Object ID of the AAD Admin
    * TF attribute: object_id
    * EnvVar in pipeline YAML: TF_VAR_azure_sql_aad_object_id
    * Bitbucket repository variables: D/Q/P_AZURE_SQL_AAD_OBJECT_ID

### Azure SQL Database ([Terraform documentation](https://www.terraform.io/docs/providers/azurerm/r/sql_database.html))
This resource needs to have user specific repository variables being defined and [passed over as an environment variable](https://amconfluence.slcloud.ch/display/AXS/Terraform+Repositories+at+SwissLife).
* Max DB size in bytes (optional parameter)
    * TF attribute: max_size_bytes
    * EnvVar in pipeline YAML: TF_VAR_max_db_size
    * Bitbucket repository variables: D/Q/P_AZURE_SQL_MAX_DB_SIZE