#usually you do not need to edit this file unless you start having multiple modules or need more environment variables
#you should be coding you infrastructure as one (or multiple) modules. These are programmed via the main.tf in the
#/modules/ folder
module "main_application" {
  source = "./modules/main_application"

  #******************************************************************
  # Start here with editing: Project specific variables             *
  #******************************************************************
  # Azure SQL Server
  azure_sql_admin_pw = var.azure_sql_admin_pw
  
  # Azure SQL AAD Admin
  azure_sql_aad_admin_email = var.azure_sql_aad_admin_email
  azure_sql_aad_object_id = var.azure_sql_aad_object_id
  
  # Azure SQL Database
  max_db_size = var.max_db_size
  
  #******************************************************************
  # End here with edititng: Project specific variables              *
  #******************************************************************
}